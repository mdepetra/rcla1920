/**
 * 
 * Assegnamento 2 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modelizza il tempo di apertura dell'ufficio postale
public class MyTimer implements Runnable{

	private Long time;

	// Costruttore
	public MyTimer(int max, int min) {
		this.time = (long)Math.floor(Math.random() * (max - min + 1) + min);
	}

	@Override
	public void run() {
		try {
			Thread.sleep(time);
			System.out.println("L'Ufficio Postale STA CHIUDENDO.");
		} catch (InterruptedException e) {
			System.out.println("Thread Timer Interrotto"); 
		}
	}

	
}
