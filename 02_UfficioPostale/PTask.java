/**
 * 
 * Assegnamento 2 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modelizza una persona come un task
public class PTask implements Runnable{

	private int id;
	private Long duration;
	
	// Costruttore
	public PTask(int id) {
		this.id = id; 
		this.duration = (long)Math.floor(Math.random() * (1000 - 500 + 1) + 500);
	}
	
	@Override
	public void run() {
		Thread t = Thread.currentThread(); 
		System.out.printf("%s: PTask %s è allo sportello per %d ms\n", t.getName(), this.id, duration);
		try{
			Thread.sleep(duration);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
		System.out.printf("%s: PTask %s ha terminato.\n", t.getName(),this.id);
	}

}
