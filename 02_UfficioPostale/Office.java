import java.util.concurrent.*;

/**
 * 
 * Assegnamento 2 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modelizza l'ufficio postale 
public class Office {
	
	private static final int nSportelli = 4;
	private static final long keepAliveTime = 50;
	private ThreadPoolExecutor sportelli;
	private ArrayBlockingQueue<Runnable> coda2;
	
	// Costruttore
	public Office (int k) {
		this.coda2  = new ArrayBlockingQueue<Runnable>(k);
		this.sportelli = new ThreadPoolExecutor(nSportelli, nSportelli, keepAliveTime, TimeUnit.SECONDS, this.coda2);

		System.out.println("L'Ufficio Postale è aperto.");
	}
	
	// Esegue un PTask
	public void executePTask (PTask p) {
		this.sportelli.execute(p);
	}
	
	// Chiude l'ufficio postale
	public void closeOffice() {
		this.sportelli.shutdown();

		try { 
			this.sportelli.awaitTermination(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			System.out.println("ATTENZIONE: Gli sportelli non hanno finito in 60 secondi!");
		}
		
		System.out.println("L'Ufficio Postale è chiuso.");
	}
	
	// Metodo che restituisce il numero di persone nella sala2
	public int getCoda2Size() {
		return this.coda2.size();
	}
}
