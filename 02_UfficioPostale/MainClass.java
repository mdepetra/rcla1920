import java.util.concurrent.LinkedBlockingQueue;

/**
 * 
 * Assegnamento 2 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

public class MainClass {

	public static void main(String[] args) {
		// MainClass flusso
		// flusso		(int) flusso=0 -> prefissato; flusso=1 -> flusso continuo in intero (poi si calcola la potenza)

		boolean flusso = false;
		
		// Se il primo argomento è 1 si genera un flusso continuo
		if (Integer.parseInt(args[0])==1) {
			flusso = true;
		}
		
		int dimSala1 = 200;		// dimensione della sala numero 1 delle poste (sala esterna)
		int dimSala2 = 10;		// dimensione della sala numero 2 delle poste (sala interna)
		int maxRand = 60000;	// max tempo di apertura delle poste 
		int minRand = 10000;	// min tempo di apertura delle poste
		
		//Coda della sala1
		LinkedBlockingQueue sala1 = new LinkedBlockingQueue<PTask>(dimSala1);
		
		Office myPO = new Office(dimSala2);
		
		if (flusso) {
			// Flusso continuo
			
			// Definisco un oggetto MyTimer per il tempo di apertura dell'ufficio (o tempo massimo per cui si accettano Persone)
			MyTimer openingTime = new MyTimer(maxRand, minRand);
			Thread tOpen = new Thread(openingTime);
			tOpen.start();
			
			int i=0;
			
			while(tOpen.isAlive()) {
				if (sala1.size()<dimSala1) {
					// Generazione di un task
					PTask p = new PTask(i);
					try {
						// Inserimento nella coda della sala1
						sala1.put(p);
						i++;
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				// Esecuzione dei task se la sala2 ha almeno un posto libero e se ci sono persone in attesa nella sala1
				if (myPO.getCoda2Size()<dimSala2 && !sala1.isEmpty()) {
					try {
						myPO.executePTask((PTask) sala1.take());
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} else {
			
			// Flusso prefissato
			Long numero = (long)(Math.random() * dimSala1);
			
			// Generiamo un numero di Persone/Task 
			for (int i = 0; i < numero; i++) {
				PTask p = new PTask(i);
				try {
					sala1.put(p);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		// Non vengono più accettati Task, si consumano solo quelli già presenti
		while(!sala1.isEmpty()) {
			if (myPO.getCoda2Size()<dimSala2) {
				try {
					myPO.executePTask((PTask) sala1.take());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		// Chiusura dell'ufficio
		myPO.closeOffice();
	}

}
