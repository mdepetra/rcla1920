/**
 * 
 * Assegnamento 3 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che gestisce l'accesso al reparto da parte dei pazienti che devono effettuare le visite
public class WardHandler {
	public static final int numDottori = 10;
	private int busyDoc = 0;
	private int redCount = 0;
	private int gialliInAttesa[] = new int[numDottori];
	private boolean docs[] = new boolean[numDottori];
	
	public WardHandler() {
		for(int i=0; i<numDottori; i++) {
			this.docs[i] = false;
			this.gialliInAttesa[i] = 0;
		}
	}

	// Metodo sysynchronized per il paziente in codice Rosso
	public synchronized void getAllDocs() {
		this.redCount++;
		
		// Attesa fino a quando tutti i medici diventano disponibili
		while (this.busyDoc > 0) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		// Si prendono tutti i medici
		for(int i=0; i<numDottori; i++) 
			this.docs[i] = true;
		
		this.busyDoc = numDottori;
	}

	// Metodo sysynchronized per il paziente in codice Rosso
	public synchronized void freeAllDocs() {
		// Si liberano tutti i medici
		for(int i=0; i<numDottori; i++) 
			this.docs[i] = false;
		
		this.busyDoc=0;
		this.redCount--;
		notifyAll();
	}

	// Metodo sysynchronized per il paziente in codice Giallo
	public synchronized void getIDoc(int iDoc) {
		this.gialliInAttesa[iDoc]++;
		// Attesa finp a quando ci sono rossi in attesa o il medico iDoc non è disponibile
		while (redCount != 0 || this.docs[iDoc]) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.docs[iDoc] = true;
		this.busyDoc++;
	}

	// Metodo sysynchronized per il paziente in codice Giallo
	public synchronized void freeIDoc(int iDoc) {
		// Si libera il medico iDoc
		this.docs[iDoc] = false;
		this.gialliInAttesa[iDoc]--;
		this.busyDoc--;
		notifyAll();
	}
	
	// Metodo sysynchronized per il paziente in codice Bianco
	public synchronized int getDoc(){
		while(true){
			// Attesa fino a quando ci sono rossi in attesa o i medici sono tutti occupati
			while(this.redCount != 0 || (this.busyDoc == numDottori))
				try {
					wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Si cerca e si assegna il primo medico disponibile
			for(int i = 0; i < numDottori; i++){
				if(!this.docs[i] && this.gialliInAttesa[i] == 0){
		            this.docs[i] = true;
		            this.busyDoc++;
		            return i;
				}
			}
			
			// Aspetto un medico disponibile
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	// Metodo sysynchronized per il paziente in codice Bianco
	public synchronized void freeDoc(int i){
		// Si libera il medico i
		this.docs[i] = false;
		this.busyDoc--;
		notifyAll();
	}
}
