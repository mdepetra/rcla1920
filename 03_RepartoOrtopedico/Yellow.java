/**
 * 
 * Assegnamento 3 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modelizza un paziente in codice GIALLO del Reparto di Ortopedia
public class Yellow extends Patient{
	
	private int iDoc;

	public Yellow(WardHandler WH, int id, int iDoc) {
		super(WH, "Y"+id);
		this.iDoc = iDoc;
	}

	@Override
	public void getDoc() {
		this.WH.getIDoc(this.iDoc);
	}

	@Override
	public void freeDoc() {
		this.WH.freeIDoc(this.iDoc);
	}

}
