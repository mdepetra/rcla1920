/**
 * 
 * Assegnamento 3 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modelizza un paziente in codice ROSSO del Reparto di Ortopedia
public class Red extends Patient{
	
	public Red (WardHandler WH, int id) {
		super(WH, "R"+id);
	}

	@Override
	public void getDoc() {
		this.WH.getAllDocs();
	}

	@Override
	public void freeDoc() {
		this.WH.freeAllDocs();
	}
}
