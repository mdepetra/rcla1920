/**
 * 
 * Assegnamento 3 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modelizza un paziente in codice BIANCO del Reparto di Ortopedia
public class White extends Patient{
	
	private int idDoc;

	public White(WardHandler WH, int id) {
		super(WH, "W"+id);
	}

	@Override
	public void getDoc() {
		// Richiesta del primo medico disponibile
		this.idDoc = this.WH.getDoc();
	}

	@Override
	public void freeDoc() {
		this.WH.freeDoc(idDoc);
	}
}
