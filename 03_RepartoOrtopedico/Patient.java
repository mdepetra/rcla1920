/**
 * 
 * Assegnamento 3 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modelizza un paziente generico del Reparto di Ortopedia
public abstract class Patient extends Thread{
	
	protected WardHandler WH;
    protected String id;
    protected int kVisite;
    protected int kCorrente;
    public static final int minVisite = 1;
    public static final int maxVisite = 10;
    
    public Patient(WardHandler WH, String id){
    	//Generazione di un numero random di visite
    	this.kVisite = (int)((Math.random() * maxVisite) + 1);
    	this.kCorrente = 0;
    	this.WH = WH;
    	this.id = id;
    }
    
    public abstract void getDoc();
    
    public abstract void freeDoc();

	public void run() {
		while(this.kCorrente < this.kVisite){
			// Richiesta di un dottore per la visita
			this.getDoc();
			
			// Visita
			try{
				Long tVisita = (long) ((Math.random() * (100)) + 1);
				System.out.println("Paziente " + this.id + " fa la visita n " + (this.kCorrente+1) + " di " + this.kVisite + "(" + tVisita+ " ms)");
				Thread.sleep(tVisita);
			}catch(InterruptedException e) {
				System.out.println(e);
			}
			this.kCorrente++;
			
			// Termina la visita e libera il dottore
			this.freeDoc();
			
			// Tempo di attesa tra una visita e la successiva
			try{
				Long tAttesaVisita = (long) ((Math.random() * (100)) + 1);
				Thread.sleep(tAttesaVisita);
			}catch(InterruptedException e) {
				System.out.println(e);
			}
		}
		
	}

}
