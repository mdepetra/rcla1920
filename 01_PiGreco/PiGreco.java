/**
 * Assegnamento 1 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * Mirko De Petra
 */

// Classe per il calcolo di PiGreco con la serie di Gregory-Leibniz 
public class PiGreco implements Runnable{
	
	// accuracy		(double) accuracy in decimale
	private double accuracy;
	
	// Costruttore
	public PiGreco (double accuracy) {
		this.accuracy=accuracy; 
	}

	@Override
	public void run() {
		double myPI = 0.0; //Memorizza la stima di pi greco
		int it = 0; //Variabile utile per la stima
		int sgn = -1; //Variabile utile per la stima
		
		System.out.println("Valore di Math.PI : " + Math.PI);
		
		// Ciclo fino a quando non si raggiunge l'accuracy desiderata e non si è interrotti
		while(Math.abs(myPI - Math.PI) > accuracy && !Thread.currentThread().isInterrupted())
		{
			it++;
			sgn = -sgn;
			myPI += sgn * 4.0 / ((2 * it) - 1);
		}
		
		// Controllo sul raggiungimento dell'accuracy
		if(Math.abs(myPI - Math.PI) > accuracy)
			System.out.println("Interrotto nel thread");
		else 
			Thread.currentThread().interrupt();
		
		System.out.println("Valore stimato con la serie di Gregory-Leibniz : " + myPI + " " +it);
		
	}
}

