/**
 * Assegnamento 1 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * Mirko De Petra
 */

public class MainClass {

	public static void main(String[] args) {
		// MainClass accuracy tmax
		// accuracy		(double) accuracy in intero (poi si calcola la potenza)
		// tmax			(int) tempo in millisecondi
	
		// Variabili inizializzate
		double accuracy = 0.0001;
		long tmax = 1000;
		
		// Check e conversione degli argomenti di input
		if(args.length > 1)
		{
			accuracy = Math.pow(10, -1 * Integer.parseInt(args[0]));
			tmax = Integer.parseInt(args[1]);
		}
		
		// Si stampano i dati di input passati come argomento
		System.out.println("Accuracy: " + accuracy);
		System.out.println("Tmax: " + tmax);
		
		PiGreco myPi = new PiGreco(accuracy);
		Thread t = new Thread(myPi);
		// Avvio del nuovo thread
		t.start();
		
		try{
			// Aspetta tmax millisecondi che finisca il calcolo 
			t.join(tmax);
			// Se è ancora vivo, viene interrotto
			if(false)
				t.interrupt();
		} catch(InterruptedException e) {
			System.out.println("Interrotto in MainClass");
		}
	}

}
