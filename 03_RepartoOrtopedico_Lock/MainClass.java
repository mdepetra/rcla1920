import java.util.ArrayList;

/**
 * 
 * Assegnamento 3 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

public class MainClass {

	public static void main(String[] args) {
		// MainClass nBianchi nGialli nRossi
		// 	nBianchi	(int) numero di pazienti in codice bianco
		//	nGialli		(int) numero di pazienti in codice giallo
		//	nRossi		(int) numero di pazienti in codice rosso
		
		// Parsing in interi dei 3 argomenti
		int nBianchi = Integer.parseInt(args[0]);
		int nGialli = Integer.parseInt(args[1]);;
		int nRossi = Integer.parseInt(args[2]);;
		
		System.out.println("Lancio " + nRossi + " pazienti in codice ROSSO");
		System.out.println("Lancio " + nGialli + " pazienti in codice GIALLO");
		System.out.println("Lancio " + nBianchi + " pazienti in codice BIANCO");
		
		// Creazione del gestore del reparto
		WardHandler WH = new WardHandler();
		
		// Creazione di una lista di pazienti con capacità iniziale nBianchi+nGialli+nRossi
		ArrayList<Patient> lst = new ArrayList<Patient>(nBianchi+nGialli+nRossi);
		
		// Creazione di nRossi pazienti in codice rosso
		for(int i=0;i<nRossi;i++) {
			Red r = new Red(WH, i+1);
			lst.add(r);
		}
		
		// Creazione di nGialli pazienti in codice giallo
		for(int i=0;i<nGialli;i++) {
			Yellow y = new Yellow(WH, i+1,((int)(Math.random() * 9)));
			lst.add(y);
		}
		
		// Creazione di nBianchi pazienti in codice bianco
		for(int i=0;i<nBianchi;i++) {
			White w = new White(WH, i+1);
			lst.add(w);
		}
		
		// Lancio dei pazienti nella lista
		for (int i=0;i<lst.size();i++) {
			lst.get(i).start();
		}
	}
}
