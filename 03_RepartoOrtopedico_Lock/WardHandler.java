import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 
 * Assegnamento 3 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

//Classe che gestisce l'accesso al reparto da parte dei pazienti che devono effettuare le visite
public class WardHandler {
	
	private ReentrantLock lock;
	private Condition rCond;
	private Condition wCond;
	private Condition[] conds;
	private int busyDoc = 0;
	public static final int numDottori = 10;
	private boolean docs[] = new boolean[numDottori];
	
	public WardHandler() {
		this.lock = new ReentrantLock();
		
		// Creazione delle variabili condizioni
		this.rCond = lock.newCondition();
		this.wCond = lock.newCondition();
		
		this.conds = new Condition[numDottori];
		
		for(int i=0; i<numDottori; i++) {
			conds[i]=lock.newCondition();
		}
		
		for (int i=0; i<numDottori;i++){
			this.docs[i] = false;
	   	}
	}

	// Metodo per il paziente in codice Rosso
	public void getAllDocs() {
		lock.lock();
		try {
			try{
				// Attesa fino a quando tutti i medici diventano disponibili
				while (this.busyDoc > 0)
					rCond.await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// Si prendono tutti i medici
			for(int i=0; i<numDottori; i++) 
				this.docs[i] = true;
			
			this.busyDoc = numDottori;
		} finally {
			lock.unlock();
		}
	}

	// Metodo per il paziente in codice Rosso
	public void freeAllDocs() {
		lock.lock();
		try {
			// Si liberano tutti i medici
			for(int i=0; i<numDottori; i++) 
				this.docs[i] = false;
			
			this.busyDoc=0;
			
			if (lock.hasWaiters(rCond)) {
				rCond.signal();
			} else {
				for (int i = 0; i< conds.length; i++)
					conds[i].signal();
				
				wCond.signalAll();
			}
		} finally {
			lock.unlock();
		}
	}
	
	// Metodo per il paziente in codice Giallo
	public void getIDoc(int iDoc) {
		lock.lock();
		try {
			try {
				// Attesa finp a quando ci sono rossi in attesa o il medico iDoc non è disponibile
				while (lock.hasWaiters(rCond) || this.docs[iDoc])
					conds[iDoc].await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			this.docs[iDoc] = true;
			this.busyDoc++;
		} finally {
			lock.unlock();
		}
	}

	// Metodo per il paziente in codice Giallo
	public void freeIDoc(int iDoc) {
		lock.lock();
		try {
			// Si libera il medico iDoc
			this.docs[iDoc] = false;
			this.busyDoc--;
			
			if(lock.hasWaiters(rCond)) {
				rCond.signal();
			} else if(lock.hasWaiters(conds[iDoc])) {
				conds[iDoc].signal();
			} else {
				wCond.signal();
			}
		} finally {
			lock.unlock();
		}
	}
	
	// Metodo per il paziente in codice Bianco
	public int getDoc(){
		lock.lock();
		try {
			while (true) {
				try {
					// Attesa fino a quando ci sono rossi in attesa o i medici sono tutti occupati
					while (lock.hasWaiters(rCond) || (this.busyDoc == numDottori))
						wCond.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			
				// Si cerca e si assegna il primo medico disponibile
				for(int i = 0; i < numDottori; i++){
					if(!this.docs[i] && !lock.hasWaiters(conds[i])){
			            this.docs[i] = true;
			            this.busyDoc++;
			            return i;
					}
				}
			}
		} finally {
			lock.unlock();
		}		
	}
	
	// Metodo per il paziente in codice Bianco
	public void freeDoc(int i){
		this.freeIDoc(i);
		/*lock.lock();
		try {
			this.docs[i] = false;
			this.busyDoc--;
			if(lock.hasWaiters(rCond)) {
				rCond.signal();
			} else if(lock.hasWaiters(conds[i])) {
				conds[i].signal();
			} else {
				wCond.signalAll();
			}
		} finally {
			lock.unlock();
		}*/
	}
}
