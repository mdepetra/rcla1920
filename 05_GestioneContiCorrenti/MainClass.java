import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class MainClass {
	public static void main(String[] args) {
		String fileName = "CA.json";
		// Generazione del file JSON
		generaFileJSON(fileName);
		
		LinkedBlockingQueue<JSONObject> listaCA = new LinkedBlockingQueue<JSONObject>();
		
		// Creazione e attivazione del Reader
		CAReader reader = new CAReader(listaCA,fileName);
		reader.start();
		
		// Creazione di un oggetto di Occorrenza
		Occorrenza contatori = new Occorrenza(); 
		
		// Creazione di un threadpool 
		TPool tPool = new TPool();
	    
	    // Ciclo fino a quando la lettura non è completata e ci sono elementi nella lista da analizzare
	    while (!reader.readNotEnd()|| !listaCA.isEmpty()) {
			try {
				JSONObject obj = listaCA.take();
				if(obj != null) {
					// Creazione di un CATask
					CATask ca = new CATask(obj,contatori);
					// Esecuzione del Task
					tPool.executeCATask(ca);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	    
	    // Chiusura del ThreadPool
		tPool.closePool();
		// Stampa delle occorrenze
		contatori.printOccorrenze();
	}
	
	// metodo statico che genera il file json
	private static void generaFileJSON(String fileName) {
		String causali[] = new String[] {"Bonifico", "Accredito", "Bollettino", "F24", "PagoBancomat"};
		String nomi[] = new String[] {"Name1", "Name2", "Name3"};
		String cognomi[] = new String[] {"Surname1", "Surname2", "Surname3", "Surname4", "Surname5", "Surname6", "Surname7"};
		int kYear=2;
		// Generazione del numero di conti correnti da creare
		int nAccount = (int) ((Math.random()*100)+1);
		
		try {
			// Creazione del JSONArray
			JSONArray myArray = new JSONArray();
			
			int nMovimentiEffettuati = 0;
			
			// Ciclo che crea i conti correnti
			for (int i=0;i<nAccount; i++) {
				// Creazione del JSONObject
				JSONObject myObj = new JSONObject();
				
				// Scelta di un nome e di un cognome
				String nome = nomi[(int)(Math.random()*nomi.length)];
				String cognome = cognomi[(int)(Math.random()*cognomi.length)];
				
				// Inserimento nell'oggetto 
				myObj.put("name", nome+" "+cognome);
				myObj.put("NumeroCC", "IT"+i);
				
				// Generazione del numero di movimenti da creare
				int nMovimenti= (int) ((Math.random()*1000)+1);
				JSONArray movimenti = new JSONArray();
				
				// Formattazione e calcolo delle date dei movimenti
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
				LocalDate today = LocalDate.now();
				long end = today.toEpochDay();
				LocalDate startDate = today.minusYears(kYear);
				
				// Ciclo che crea i movimenti
				for (int i1=0;i1<nMovimenti;i1++){
					JSONObject movimentoT = new JSONObject();
					long start = startDate.toEpochDay();
				    
					long randomEpochDay = ThreadLocalRandom.current().longs(start, end).findAny().getAsLong();
				    startDate = LocalDate.ofEpochDay(randomEpochDay);
					movimentoT.put("date", formatter.format(LocalDate.ofEpochDay(randomEpochDay)));
					movimentoT.put("causale", causali[(int)(Math.random()*causali.length)]);
					
					double balance = (Math.random()*10)*1000;
					movimentoT.put("importo",balance);
					movimenti.add(movimentoT);
					nMovimentiEffettuati++;
				}
				// Inserimento dei movimenti nell'oggetto
				myObj.put("movimenti",movimenti);
				// Inserimento dell'oggetto nell'array
				myArray.add(myObj);
			}
			
			System.out.println("Il numero di conti correnti creati sono " + nAccount);
			System.out.println("Il numero di movimenti totali creati sono " + nMovimentiEffettuati);
			
			ByteBuffer buffer = ByteBuffer.wrap(myArray.toJSONString().getBytes());
			
			// Cancello il file se esite
			Files.deleteIfExists(Paths.get(fileName)); 
			
			// Creazione del nuovo file
			Files.createFile(Paths.get(fileName)); 
			
			// Creazione del Channel per la scrittura
			FileChannel outChannel = FileChannel.open(Paths.get(fileName), StandardOpenOption.WRITE);
			
			while(buffer.hasRemaining()) {
				outChannel.write(buffer);
			}
			
			outChannel.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

}
