/**
 * 
 * Assegnamento 5 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modelliza le occorrenze delle causali
public class Occorrenza {

	private static int nCA=0;
	private static int nBonifici=0;
	private static int nAccrediti=0;
	private static int nBollettini=0;
	private static int nF24=0;
	private static int nPagoBancomat=0;
	
	public Occorrenza (){
		
	}

	// Metodo che in base alla causale sceglie il metodo da invocare per incrementare il contatore
	public void increment(String causale) {
		
		switch (causale) {
	        case "Bonifico":
	        	incrementBonifici();
	            break;
	        case "Accredito":
	       	 	incrementAccrediti();
	            break;
	        case "Bollettino":
	       	 	incrementBollettini();
	            break;
	        case "F24":
	       	 	incrementF24();
	            break;
	        case "PagoBancomat":
	       	 	incrementPagoBancomat();
	            break;
		}
	}
	
	// Incrementa il numero di conti correnti
	public static synchronized void incrementCA (){
		nCA++;
	}
	
	// Incrementa il numero di bonifici
	public static synchronized void incrementBonifici (){
		nBonifici++;
	}
	
	// Incrementa il numero di accrediti
	public static synchronized void incrementAccrediti (){
		nAccrediti++;
	}
	
	// Incrementa il numero di vollettini
	public static synchronized void incrementBollettini (){
		nBollettini++;
	}
	
	// Incrementa il numero di F24
	public static synchronized void incrementF24 (){
		nF24++;
	}
	
	// Incrementa il numero di pagobancomat
	public static synchronized void incrementPagoBancomat (){
		nPagoBancomat++;
	}
	
	// Metodo che stampa le occorrenze 
	public void printOccorrenze() {
		int k =0;
		System.out.println("Bonifici:" + nBonifici);
        k = k + nBonifici;
        System.out.println("Accrediti:" + nAccrediti);
        k = k + nAccrediti;
        System.out.println("Bollettini:" + nBollettini);
        k = k + nBollettini;
        System.out.println("F24:" + nF24);
        k = k + nF24;
        System.out.println("PagoBancomat:" + nPagoBancomat);
        k = k + nPagoBancomat;

		System.out.println("Numero di Movimenti effettuati: " + k);
		System.out.println("Numero di CC letti: " + nCA);
	}

}
