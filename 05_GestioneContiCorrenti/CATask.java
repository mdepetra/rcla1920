import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * 
 * Assegnamento 5 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

// Classe che modelliza un task per l'analisi delle occorrenze nel conto corrente 
public class CATask implements Runnable{
	
	private JSONObject obj;
	private Occorrenza contatori;
	
	// Costruttore
	public CATask(JSONObject obj, Occorrenza contatori) {
		this.obj = obj;
		this.contatori = contatori;
	}

	@Override
	public void run() {
		// Estrazione dell'elenco dei movimenti
		JSONArray movimenti = (JSONArray) obj.get("movimenti");
		for (Object obj: movimenti) {
			JSONObject movimento = (JSONObject) obj;
			
			// Estrazione della causale e incremento dei dati
			String causale = (String) movimento.get("causale");
			contatori.increment(causale);
		}
		// Incremento del numero di conti correnti analizzati
		Occorrenza.incrementCA();
	}

}
