import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {

	public static void main(String[] args) {
		// Server port
		
		// Si controlla  il numero di argomenti ricevuti da linea di comando
		if (args.length == 0) {
			System.out.println("Usage: java Server port"); 
			return; 
		}
		
		int port;
		
		// Parsing dell'argomento per la porta e controllo di correttezza
		try {
			port = Integer.parseInt(args[0]);
		} catch (RuntimeException ex) {
			System.out.println("ERR -arg 2"); 
			return; 
		}
		
		try {
			// Creazione di un'istanza dell'oggetto CongressoService
			CongressoServiceImpl congressoService = new CongressoServiceImpl();
			
			// Esportazione dell'oggetto
			CongressoService stub = (CongressoService) UnicastRemoteObject.exportObject(congressoService, 0);
			
			// Creazione di un registry sulla porta port
			LocateRegistry.createRegistry(port);
			Registry r = LocateRegistry.getRegistry(port);
			
			// Pubblicazione dello stub nel registry
			r.rebind("CONGRESSO-SERVER", stub);
			
			System.out.println("Server pronto");
			
		} catch (RemoteException e) {
			System.out.println("Communication error " + e);
			e.printStackTrace();
		}
	}

}
