import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface CongressoService extends Remote {
	
	public int speakerRegistration(String speakerName, int day, int sessionNo) throws RemoteException;
	
	public String getConferenceAgenda(int day) throws RemoteException;
	
	public String getConferenceAgenda() throws RemoteException;
	
	public List<String> getCodeErrorList() throws RemoteException;

	public List<String> getListOps() throws RemoteException;
	
	public String getDays() throws RemoteException;
	
	public String getSessioni() throws RemoteException;
}
