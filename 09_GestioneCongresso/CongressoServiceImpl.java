import java.rmi.RemoteException;
import java.rmi.server.RemoteServer;
import java.util.ArrayList;
import java.util.List;

public class CongressoServiceImpl extends RemoteServer implements CongressoService{
	private static int NUM_DAY = 3;
	private static int NUM_SESSIONI = 12;
	private static final int SUCCESS = 0;
	private static final int ERR_INSERT = 1;
	private static final int ERR_DAY = 2;
	private static final int ERR_SESSION = 3;
	private static final int ERR_FULLSESSION = 4;
	/**
	 * Codice 0: intervento aggiunto con successo
	 * Codice 1: errore nell'inserimento dell'intervento
	 * Codice 2: Giorno inesistente
	 * Codice 3: Sessione inesistente
	 * Codice 4: Sessione 
	 * Codice 5: errore non definito
	 */
	private ArrayList<DayAgenda> agenda;
	private List<String> listaOps;
	private List<String> errori; 

	public CongressoServiceImpl () throws RemoteException {
		this.agenda = new ArrayList<DayAgenda>(NUM_DAY);
		
		for (int i=0; i<NUM_DAY; i++)
			this.agenda.add(new DayAgenda(i+1,NUM_SESSIONI));
		
		this.listaOps = new ArrayList<String>();
		
		this.listaOps.add("Inserire uno speaker");
		this.listaOps.add("Visualizza Agenda COMPLETA del Congresso");
		this.listaOps.add("Visualizza Agenda PER UN GIORNO del Congresso");
		
		this.errori = new ArrayList<String>();
		
		this.errori.add("Intervento aggiunto con successo");
		this.errori.add("Intervento NON inserito");
		this.errori.add("Il giorno è INESISTENTE.");
		this.errori.add("La sessione è INESISTENTE.");
		this.errori.add("La sessione ha tutti gli spazi d'intervento coperti.");
		this.errori.add("Errore indefinito.");
 	}
	
	
	@Override
	public int speakerRegistration(String speakerName, int day, int sessionNo) throws RemoteException {
		if (day>NUM_DAY || day<0) 
			return ERR_DAY;
		
		int result = agenda.get(day-1).addTalk(speakerName, sessionNo-1);
			
		switch (result) {
			case SUCCESS:
				System.out.println("Intervento " + speakerName + " inserito con successo.");
				break;
			case ERR_INSERT:
				System.out.println("Intervento " + speakerName + " NON inserito.");
				break;
			case ERR_SESSION:
				System.out.println("La sessione " + sessionNo + " è INESISTENTE.");
				break;
			case ERR_FULLSESSION:
				System.out.println("La sessione " + sessionNo + " ha tutti gli spazi d'intervento coperti.");
				break;
			default:
				System.out.println("Si è verificato un errore.");
				break;
			
		}
		
		return result;
	}

	@Override
	public String getConferenceAgenda(int day) throws RemoteException {
		return (day>NUM_DAY || day<0) ? errori.get(ERR_DAY) : agenda.get(day-1).getDayAgenda();
	}
	
	@Override
	public String getConferenceAgenda() throws RemoteException {
		String stringAgenda = "";
		
		for (DayAgenda day : agenda) {
			int index = agenda.indexOf(day);
			stringAgenda += "Giorno n. " + agenda.get(index).getNDay() + "\n" + agenda.get(index).getDayAgenda();
		}
		
		return stringAgenda;
	}

	@Override
	public List<String> getCodeErrorList() throws RemoteException {
		List<String> l = new ArrayList<String>();
		l.addAll(this.errori);
		return l;
	}

	@Override
	public List<String> getListOps() throws RemoteException {
		List<String> l = new ArrayList<String>();
		l.addAll(this.listaOps);
		return l;
	}

	@Override
	public String getDays() throws RemoteException {
		List<Integer> l = new ArrayList<Integer>();
		
		for (int i=0; i<NUM_DAY; i++)
			l.add(i+1);
		
		return l.toString();
	}

	@Override
	public String getSessioni() throws RemoteException {
		List<Integer> l = new ArrayList<Integer>();
		
		for (int i=0; i<NUM_SESSIONI; i++)
			l.add(i+1);
		
		return l.toString();
	}
	
	

}
