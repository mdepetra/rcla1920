import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.Scanner;

public class Client {
	
	public static void main(String[] args) {
		// Client ip-host
		//	ip-host		(int) porta a cui collegarsi
		
		// Si controlla  il numero di argomenti ricevuti da linea di comando
		if (args.length == 0) {
			System.out.println("Usage: java Client port"); 
			return; 
		}
		
		int port;
		
		// Parsing dell'argomento per la porta e controllo di correttezza
		try {
			port = Integer.parseInt(args[0]);
		} catch (RuntimeException ex) {
			System.out.println("ERR -arg 1"); 
			return; 
		}
		
		
		// Controllo il numero degli argomenti
		CongressoService serverObject;
		Remote remoteObject;
		
		boolean status = true;
		
		try {
			Registry r = LocateRegistry.getRegistry(port);
			remoteObject = r.lookup("CONGRESSO-SERVER");
			serverObject = (CongressoService) remoteObject;
			
			// Creazione dello Scanner per leggere dalla console
			Scanner stdin = new Scanner(System.in);
			
			List<String> errori = serverObject.getCodeErrorList();
			
			// Richiesta al client di registrare una nuova sessione
			do {
				
				System.out.println("Operazioni: \n" + getListOps(serverObject));
				System.out.print("\nScegliere l'operazione da effettuare: ");
				int idOp = Integer.parseInt(stdin.nextLine());
				
				switch (idOp) {
					case 0:
						System.out.println(errori.get(inserisciSpeaker(stdin,serverObject)));
						break;
					case 1:
						System.out.println("\tStampa agenda\n\n"+serverObject.getConferenceAgenda());
						break;
					case 2:
						System.out.print("\nInserire il giorno: ");
						int day = Integer.parseInt(stdin.nextLine());
						System.out.println("\tStampa agenda\n\n"+serverObject.getConferenceAgenda(day));
						break;
					case 3:
						System.out.println("Terminazione in corso...");
						status = false;
						break;
					default:
						System.out.println("Operazione non riconosciuta.");
						break;
				}
			} while(status);
			//test(serverObject);
			//System.out.println("\tStampa agenda\n\n"+serverObject.getConferenceAgenda());
			System.out.println("END");
		} catch (Exception e) {
			System.err.println("Error in invoking object method " + e.toString());
		}
	}
	
	private static int inserisciSpeaker(Scanner stdin, CongressoService serverObject) throws RemoteException {
		System.out.print("Inserire il nome dello speaker: ");
		String speakerName = stdin.nextLine();
		String days = serverObject.getDays();
		System.out.print("Inserire il giorno " + days +" : ");
		int day = Integer.parseInt(stdin.nextLine());
		String sessioni = serverObject.getSessioni();
		System.out.print("Inserire la sessione " + sessioni + " : ");
		int sessionNo = Integer.parseInt(stdin.nextLine());
		
		int result = serverObject.speakerRegistration(speakerName, day, sessionNo);
		
		return result;
	}

	private static String getListOps(CongressoService serverObject) throws RemoteException {
		String ops = "";
		
		int i=0;
		
		List<String> listaOperazioni = serverObject.getListOps();
		
		for(String op:listaOperazioni)
			ops += "\n\t" + (i++) + " -> " + op;
		
		ops += "\n\t" + i + " -> Fine operazioni e termina programma";

		return ops;
	}

	public static void test (CongressoService serverObject) {
		String[] nomi = {"Pippo","Pluto","Paperina","Paperino","Topolino","Zio Paperone","Qui", "Quo", "Qua"};
		
		for (int k=0;k<5;k++) {
			for (int i=1;i<=3;i++) {
				for (int j=1;j<=12;j++) {
					int d = (int) (Math.random()*nomi.length);
					System.out.println(nomi[d] + i + j);
					try {
						serverObject.speakerRegistration(nomi[d], i, j);
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

}
