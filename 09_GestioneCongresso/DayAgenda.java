import java.util.ArrayList;

public class DayAgenda {
	private static int MAX_INTERVENTI = 5;

	private ArrayList<ArrayList<String>> sessioni;
	private int nDay;
	private int nSessioni;
	
	public DayAgenda(int nDay, int nSessioni) {
		this.nSessioni = nSessioni;
		sessioni = new ArrayList<ArrayList<String>>(this.nSessioni);
		this.nDay = nDay;
		for (int i=0; i<this.nSessioni; i++)
			sessioni.add(new ArrayList<String>(MAX_INTERVENTI));
	}
	
	/**
	 * Codice 0: intervento aggiunto con successo
	 * Codice 1: errore nell'inserimento dell'intervento
	 * Codice 2: Giorno non trovato
	 * Codice 3: Sessione non trovata
	 * Codice 4: errore non definito
	 */
	public int addTalk(String speakerName, int sessionNo) {
		// Se il numero di sessione è negativo o superiore a quello presente
		if (sessionNo<0 || sessionNo>this.nSessioni) 
			return 3;
		
		if (!(sessioni.get(sessionNo).size()==MAX_INTERVENTI))
			return (sessioni.get(sessionNo).add(speakerName) ? 0 :  1);
		
		return 4;
		
	}
	
	public String getDayAgenda() {
		String stringAgenda = "";
		
		for (int i=0;i<sessioni.size();i++) {
			stringAgenda += "\tSessione n. " + (i+1);
			for (Object t : sessioni.get(i).toArray())
				stringAgenda += "\t" + t.toString();
			stringAgenda += "\n";
		}
		
		return stringAgenda;
	}
	
	public int getNDay () {
		return this.nDay;
	}
}
