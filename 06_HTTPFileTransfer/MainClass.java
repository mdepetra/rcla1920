import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 
 * Assegnamento 6 del Laboratorio di Reti di Calcolatori A
 * A.A. 2019/2020
 * @author Mirko De Petra
 *
 */

public class MainClass {
	public static void main(String[] args) {
		
		int port = 6789;
		try (ServerSocket server = new ServerSocket(port)) {
			
			// Il server è in ascolto
			System.out.println("Listening for connections on port " + port);
			
			while (true) {
				// Si accetta una connessione
				Socket connection = server.accept();
				
				InputStream is = null;
				DataOutputStream binaryOut = null;
				
				try {
					InputStreamReader isr =  new InputStreamReader(connection.getInputStream());
					BufferedReader reader = new BufferedReader(isr);
					
					String resource = "";
					
					System.out.println("Request received: ");
					
					// Lettura della richiesta HTTP
					String line = reader.readLine(); 
					
					while (!line.isEmpty()) { 
						System.out.println(line);
						if(line.startsWith("GET"))
		                    resource = line;
						line = reader.readLine(); 
					}
					
					binaryOut = new DataOutputStream(connection.getOutputStream());
			        
					// Si controlla di non aver ricevuto una richiesta diversa da GET
					if (resource != "") {
						// Elaborazione della stringa per estrarre il nome del file
						String[] parts = resource.split("\\s+");
				        String filename = parts[1].substring(1);

				        File risorsa = new File(filename);
	
				        // Se il file esiste e non è una directory, viene letto e inviato al client; altrimenti viene mandato un messaggio di errore
				        if (risorsa.exists() && !risorsa.isDirectory()) {
				        	// Si legger il file
				        	is = new FileInputStream(risorsa);
						    byte[] data = new byte[(int) risorsa.length()];
					        is.read(data);
					        
					        // Si invia il file richiesto al client
					        binaryOut.writeBytes("HTTP/1.0 200 OK\r\n");
					        binaryOut.writeBytes("Content-Length: " + data.length);
					        binaryOut.writeBytes("\r\n\r\n");
					        binaryOut.write(data);
					        System.out.println("Il file " + filename + " è stato trovato e spedito al client.\n");
			        	}
			        	else {
			        		// Si invia al client un messaggio di risorsa not found
			        		binaryOut.writeBytes("HTTP/1.0 404 Not Found\r\n");
			        		binaryOut.writeBytes("\r\n\r\n");
			        		binaryOut.writeBytes("Il file " + filename + " non esiste.\r\n\r\n");
			        		System.out.println("Il file " + filename + " non è stato trovato.\n");
			        	}
			        }
					else
					{
						// Si invia al client un messaggio di richiesta non conforme
				        binaryOut.writeBytes("HTTP/1.0 501 Not Implemented\r\n");
				        binaryOut.writeBytes("Content-Type: text/html\r\n");
						binaryOut.writeBytes("\r\n\r\n");
						binaryOut.writeBytes("Ops!!");
						System.out.println("La richiesta inviata è diversa da GET.\n");
					}
					binaryOut.flush();
				} catch (IOException ex) {
					System.out.println(ex.getMessage());
				} finally {
					// Chiusura
					if (is != null)
			    		is.close();
		        	if (binaryOut!= null)
		        		binaryOut.close();
					connection.close();
				}
			}
		} catch (IOException ex) { 
			System.out.println(ex);
		}
	}
		
}
